import 'package:flutter/material.dart';

class ChangingButton extends StatelessWidget {
  final VoidCallback changeText;
  final String buttonText;

  const ChangingButton({
    Key? key,
    required this.changeText,
    required this.buttonText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        onPressed: changeText,
        child: Text(buttonText),
        color: Colors.blue,
        textColor: Colors.white,
      ),
    );
  }
}
