// 1) Create a new Flutter App (in this project) and output an AppBar and some text
// below it
// 2) Add a button which changes the text (to any other text of your choice)
// 3) Split the app into three widgets: App, TextControl & Text
import 'package:flutter/material.dart';

import './body_text.dart';
import './changing_button.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _bodyText = 'First text in body';
  var _buttonText = 'Change the text';

  void changeText() {
    setState(() {
      _bodyText = _bodyText == 'First text in body'
          ? 'The text in body is now changed!'
          : 'First text in body';

      _buttonText = _buttonText == 'Change the text'
          ? 'Set the text back'
          : 'Change the text';
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Widget basic assignment problem'),
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            children: [
              BodyText(
                bodyText: _bodyText,
              ),
              ChangingButton(
                changeText: changeText,
                buttonText: _buttonText,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ),
      ),
    );
  }
}
