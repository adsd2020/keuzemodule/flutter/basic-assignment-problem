import 'package:flutter/material.dart';

class BodyText extends StatelessWidget {
  final String bodyText;
  const BodyText({Key? key, required this.bodyText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        bodyText,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
      margin: EdgeInsets.only(bottom: 50),
    );
  }
}
